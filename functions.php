<?php
/**
 * Functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package Smores
 * @since Smores 2.0
 */

define("THEME_ROOT", get_stylesheet_directory());

// Composer dependencies
require_once __DIR__ . '/lib/vendor/autoload.php';

use Smores\Smores;
use Smores\TopBarPageWalker;
use Smores\TopBarWalker;

$smores = new Smores(
    array( // Includes
        'lib/admin',         // Add admin scripts
        'lib/ajax',          // Add ajax scripts
        'lib/classes',       // Add classes
        'lib/custom-fields', // Add custom field scripts
        'lib/forms',         // Add form scripts
        'lib/images',        // Add images scripts
        'lib/post-types',    // Add post type scripts
        'lib/shortcodes',    // Add shortcode scripts
        'lib/widgets',       // Add widget scripts
    ),
    array( // Assets
        'css'             => '/dist/css/styles.min.css',
//        'css'             => '/dist/css/styles.min.clean.css',
        'js'              => '/dist/js/scripts.min.js',
        'modernizr'       => '/dist/js/vendor/modernizr.min.js',
        'jquery'          => '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js',
        'jquery_fallback' => '/dist/js/vendor/jquery.min.js',
)
);

/**
 * [smores_numeric_pagination description]
 *
 * @param  [type] $custom_query [description]
 * @param  string $classes      [description]
 * @return [type]               [description]
 */
function smores_numeric_pagination($custom_query = false, $classes = '')
{
    $query = null;

    if ($custom_query) {
        $query = $custom_query;
    } else {
        global $wp_query;

        $query = $wp_query;

        if (is_singular()) {
            return;
        }
    }

    /** Stop execution if there's only 1 page */
    if ($query->max_num_pages <= 1) {
        return;
    }

    $paged = (get_query_var('paged')) ? absint( get_query_var('paged')) : 1;
    $max   = $query->max_num_pages;

    /** Add current page to the array */
    if ($paged >= 1) {
        $links[] = $paged;
    }

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo "<ul class=\"pagination {$classes}\" role=\"menubar\" aria-label=\"Pagination\">\n";

    /** Previous Post Link */
    if (get_previous_posts_link()) {
        printf("<li class=\"arrow show-for-medium-up previous-link\">%s</li>\n", get_previous_posts_link());
    } else {
        echo '<li class="arrow unavailable previous-link"><a href="#">&laquo; Previous Page</a></li>';
    }

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = ($paged === 1) ? ' class="current"' : '';

        printf("<li%s><a href=\"%s\">%s</a></li>\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links)) {
            echo '<li>…</li>';
        }
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);

    foreach ((array) $links as $link) {
        $class = ($paged === $link) ? ' class="current"' : '';

        printf("<li%s><a href=\"%s\">%s</a></li>\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links)) {
            echo "<li>…</li>\n";
        }

        $class = ($paged === $max) ? ' class="current"' : '';

        printf("<li%s><a href=\"%s\">%s</a></li>\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link()) {
        printf("<li class=\"arrow next-link\">%s</li>\n", get_next_posts_link());
    } else {
        echo '<li class="arrow unavailable show-for-medium-up next-link"><a href="#">Next Page &raquo;</a></li>';
    }

    echo "</ul>\n";
}


function register_my_menu() {
  register_nav_menu('main',__( 'Main Menu' ));

}
add_action( 'init', 'register_my_menu' );


add_image_size( 'banner',1800,9999999999999 );


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array('page_title' => 'Site Options', 'icon_url' =>'dashicons-admin-generic','position' => '2'));
	acf_add_options_page(array('page_title' => 'Documentation', 'icon_url' =>'dashicons-admin-generic'));
}



/**
*	Smores Included Plugin Installs
*   Advanced Custom Fields PRO
*   Advanced Custom Fields: Font Awesome
*   Advanced Custom Fields: Contact Form 7
*   Contact Form 7
*   EWWW Image Optimizer
*   Grunt Sitemap Generator
*   Regenerate Thumbnails
*   WP Security Audit Log
**/

add_action('after_switch_theme' , 'smores_install_plugin_pack');

// First check it make sure this function has not been done
function smores_install_plugin_pack(){
//    if (get_option('smores_plugin_installer_ran') != "yes") {
//
//        smores_run_install_plugin_pack();
//
//    // Now that function has been run, set option so it wont run again
//        update_option( 'smores_plugin_installer_ran', 'yes' );
//
//    }
    smores_run_install_plugin_pack();
}

add_action('switch_theme', 'smores_reset_installer_switch');
function smores_reset_installer_switch(){
    if(get_option('smores_plugin_installer_ran') == "yes"){
       // update_option( 'smores_plugin_installer_ran', 'no');
    }
}

function smores_run_install_plugin_pack(){
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
    require_once ABSPATH . 'wp-admin/includes/file.php';
    $zippedThemePlugins = get_template_directory() . '/smores_pu/plugins/';
    $pluginsDirectory =  get_home_path() . 'wp-content/plugins/';

    smores_check_for_and_install_plugins( $zippedThemePlugins, $pluginsDirectory );
    //smores_plugin_directory_scanner( $pluginsDirectory, $activate = true );
    smores_clean_tmp_directory( $zippedThemePlugins . 'tmp' );

    return true;
}



/**
 * Checks for installed plugins. If not previously installed. Installs and activates plugin.
 * @param  string $zippedPath           Path to zipped plugins required for the theme.
 * @param  string $installedPluginsPath Path to currently installed plugins.
 * @return bool                       true if function complets.
 */
function smores_check_for_and_install_plugins( $zippedPath, $installedPluginsPath ){
    $alreadyInstalled = array();
    $tmpPlugins = array();
    $tmpUnzipPath = $zippedPath . 'tmp/';


    if( !file_exists($tmpUnzipPath) ){
        mkdir( $tmpUnzipPath , 0774 );
    }

    if( file_exists( $tmpUnzipPath ) == false ){
        echo "ERROR, tmp directory does not exist!"; //This needs to be dealt with properly later return an error
    } else {
        smores_unpack_plugins($zippedPath , $tmpUnzipPath);

        $tmpPlugins = smores_plugin_directory_scanner($tmpUnzipPath , $activate = false);
        $alreadyInstalled = smores_plugin_directory_scanner($installedPluginsPath , $activate = false);
    }

    foreach ($tmpPlugins as $tmpPlugin) {
        $i = 1;

        if(sizeof($alreadyInstalled) == 0 ){
            $alreadyInstalled[] = array('Name' => null , 'FilePath' => null , 'DirectoryPath' => null);
        }

        foreach ($alreadyInstalled as $installedPlugin) {
            if( $installedPlugin['Name'] == $tmpPlugin['Name'] ){

                //Do Nothing
                break;

            } else {

                if($i == sizeof($alreadyInstalled)){
                    $getDirectoryToCopy = explode('/', $tmpPlugin['DirectoryPath']);
                    $directoryToCopy = array_pop($getDirectoryToCopy);
                    $fileToCopyPath = explode('/', $tmpPlugin['FilePath']);
                    $fileToCopy = '/' . array_pop($fileToCopyPath);
                    $newCompleteDirectory = $installedPluginsPath . $directoryToCopy;

                    if(!file_exists( $newCompleteDirectory)){
                        mkdir( $newCompleteDirectory);
                    }

                    smores_recursive_copy($tmpPlugin['DirectoryPath'] ,  $newCompleteDirectory);

                    $activateMe[] = $installedPluginsPath . $directoryToCopy . $fileToCopy;
                }
            }

            $i++;

        }
    }

    $pluginsToActivate = sizeof($activateMe);
    for($i=0; $i<$pluginsToActivate; $i++){
        activate_plugin($activateMe[$i]);
    }

    unset($tmpUnzipPath);
    return true;
}

/**
 * Scans the given directory for .zip files.
 * Extracts zips to specified directory
 *
 * @param string $zippedPath	Path to directory with zip files.
 * @param string $unzippedPath	Path to unzip files to.
 * @return bool true
 */
function smores_unpack_plugins( $zippedPath , $unzippedPath ){

    foreach ( scandir($zippedPath) as $zippedPluginFile ) {
        $zip = new ZipArchive();

        if ( substr($zippedPluginFile, -4) == '.zip' ){
            $zip->open($zippedPath . $zippedPluginFile );
            $zip->extractTo( $unzippedPath);
            //$zip->close; add if exists for close() method
            unset($zip);
        }
    }

    return true;
}


/**
 * Scans a given directory for php files which contain WP plugin info.
 * @param  string  $dir      Wordpress Plugins directory usually /wp-content/plugins
 * @param  boolean $activate When set to true found plugins are activated.
 * @return array           returns a multi-dementional array containing plugin Name, php file path and directory path
 */
function smores_plugin_directory_scanner($dir , $activate = false){
    $pluginInfoArray = array();

    foreach (scandir($dir) as $directoryContent) {

        if(is_dir($dir . $directoryContent)){

            if($directoryContent == '.' || $directoryContent == '..'){
                //do nothing

            } else {

                foreach (scandir($dir . $directoryContent) as $file) {

                    if (substr($file, -4) == '.php'){

                        $pluginDirectoryPath =  $dir . $directoryContent;
                        $fullPluginPath = $dir . $directoryContent  .'/'.  $file;

                        $pluginInfo =  get_plugin_data($fullPluginPath);

                        if(strlen($pluginInfo['Name']) > 0 ){
                            $pluginInfoArray[] = array('Name' => $pluginInfo['Name'] , 'FilePath' => $fullPluginPath , 'DirectoryPath' => $pluginDirectoryPath);
                            //echo $pluginInfoArray['Name'] . '<- Name<br>';
                            if($activate == true){
                                activate_plugin($fullPluginPath);
                            }
                        }

                    }

                    unset($pluginInfo);
                    unset($newActivePlugin);
                }
            }
        }
    }

    return $pluginInfoArray;

}


/**
 * Recursivly copies directories and contents to a new location.
 * @param  string  $src   Path to the source directory.
 * @param  string  $dst   Path to the destination directory.
 * @param  integer $depth A safty to avoid infinte loops.
 * @return bool true;
 */
function smores_recursive_copy( $src , $dst, $depth = 1000 ){
    global $loopStop;
    $loopStop++;
    if($loopStop > $depth){
        return;
    } else {
        $files = glob($src . "/*");
        foreach($files as $file){
            if($file != '.' || $file != '..'){
                if(is_dir($file)){
                    $fileName = explode('/' , $file);
                    $newFile = array_pop($fileName);
                    $newPath = $dst . '/' . $newFile;
                    if(!file_exists($newPath)){
                        mkdir($dst . '/' . $newFile, 0775 );
                    }

                    $newSrc = $src . '/' . $newFile;
                    $newDst = $dst . '/' . $newFile;
                    smores_recursive_copy($newSrc, $newDst);
                } else {
                    $fileName = explode('/' , $file);
                    $newFile = array_pop($fileName);
                    copy($file , $dst . '/' . $newFile);
                }
            }
        }
    }

    return true;

}

function smores_clean_tmp_directory($zippedThemePlugins){

	foreach (scandir( $zippedThemePlugins ) as $file){
		if ($file == '.' || $file == '..'){
			//do nothing
		} else {
			if (is_dir( $zippedThemePlugins . '/' . $file )){
				$newDirectoryPath = $zippedThemePlugins . '/' .$file;
				smores_clean_tmp_directory( $newDirectoryPath );
			} else {
				unlink( $zippedThemePlugins . '/' . $file );
			}
		}
	}

	rmdir( $zippedThemePlugins );

}

/**
*	END Smores Included Plugin Installs
*   ACF Pro
*   ACF Font Awesome
*   ACF CF7
*   Contact Form 7
*   EWWW Image Optimizer
*   Regenerate Thumbnails
*   WP Security Audit Log
**/



/** Smores Admin Theme Function **/

function add_favicon() {
  	$favicon_url = get_template_directory_uri() . '/admin/img/favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

// Now, just make sure that function runs when you're on the login page and admin pages
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');


function fiwi_admin_styles() {
wp_enqueue_style( 'admin', get_template_directory_uri() . '/admin/css/style.css', false, 'all' );
wp_enqueue_script( 'admin_mag', get_template_directory_uri() . '/admin/js/vendor/magnific.min.js', true, true, 'all' );
wp_enqueue_script( 'admin_scripts', get_template_directory_uri() . '/admin/js/scripts.js', true, true, 'all' );
wp_enqueue_style( 'admin_mag_css', get_template_directory_uri() . '/admin/js/vendor/magnific.css', true, true, 'all' );
}

add_action( 'admin_enqueue_scripts', 'fiwi_admin_styles' );
add_action( 'login_enqueue_scripts', 'fiwi_admin_styles' );

function fiwi_admin_fonts() {

wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700', false, 'all' );
wp_enqueue_style( 'roboto-slab', 'https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700', false, 'all' );
wp_enqueue_script( 'fa', 'https://use.fontawesome.com/c6224e36ed.js', false, 'all' );

}

add_action( 'admin_enqueue_scripts', 'fiwi_admin_fonts' );
add_action( 'login_enqueue_scripts', 'fiwi_admin_fonts' );

function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_meta' );


add_action( 'admin_menu', 'pd_ga_toback_events_add_admin_menu' );

function pd_ga_toback_events_add_admin_menu(  ) {

	add_menu_page( 'Documentation', 'Documentation', 'manage_options', 'documentation_admin_panel', 'documentation_options_page', 'http://requests.findsomewinmore.com/wp-content/plugins/admin-theme/img/admin-logo.png', '3' );

}
function documentation_options_page(  ) {

	?>

	<div id="poststuff">
	<div class="wrap acf-settings-wrap">
		<h1>Site Documentation</h1>




  <?php if( have_rows('documentation_section', 'options') ):?>
  <?php while ( have_rows('documentation_section', 'options') ) : the_row();?>
	<div id="postbox-container-2" class="postbox documentation-table">
<table class="wp-list-table widefat fixed striped">
    <thead>
        <tr>
            <th scope="col" id="event" class="manage-column column-category"><h1><?php the_sub_field('title', 'options');?></h1></th>
        </tr>
    </thead>
    <tbody id="the-list" data-wp-lists="list:activity">

  <?php if( have_rows('documentation_topic', 'options') ):?>
  <?php while ( have_rows('documentation_topic', 'options') ) : the_row();?>

        <tr>
            <td class="event column-event"><h2><a href="#<?php echo str_replace(' ', '-', get_sub_field('title', 'option')); ?>" class="topic-handle"><?php the_sub_field('title', 'options');?> <i class="fa fa-angle-down"></i></a></h2></td>
        </tr>
        <tr class="collapsed" id="<?php echo str_replace(' ', '-', get_sub_field('title', 'option')); ?>" style="display: none;">
            <td class="doc-content">
				<a href="<?php $attachment_id = get_sub_field('image','options'); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>" class="image-link" title="<?php the_sub_field('title', 'options');?>"><img class="image-doc" src="<?php $attachment_id = get_sub_field('image','options'); $size = "medium"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>"></a>
				<?php the_sub_field('content', 'options');?>
			</td>
        </tr>
  <?php endwhile; ?>
  <?php endif; ?>

    </tbody>
</table>
     </div>
  <?php endwhile; ?>
  <?php endif; ?>




</div></div>
	<?php

}

function remove_menus(){


  remove_menu_page( 'edit.php' );    //Pages
  remove_menu_page( 'edit-comments.php' );    //Pages


}
add_action( 'admin_menu', 'remove_menus' );

