/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
(function ($) {
	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.
	var Sage = {
		// All pages
		'common': {
			init: function () {
				// JavaScript to be fired on all pages
				$('.wpcf7-form-control-wrap').click(function () {
					$(this).addClass('active');
					return false;
				});
				$(".wpcf7").on('wpcf7:invalid', function (event) {
					$('.wpcf7-form-control-wrap').addClass('invalid');
				});
				$('.mobile-toggle a').click(function () {
					$('body').toggleClass('menu-open');
					return false;
				});
				paddingHelp();
				navHeight();
				$(window).resize(function () {
					if ($(window).width() > 768) {
						paddingHelp();
					}
					navHeight();
				});
				$('.match-me').matchHeight();
				bgTransition();

				function navHeight() {
					var headerHeight = $('header').outerHeight();
					$('.mobile-menu').css('top', headerHeight);
					if ($('.page-id-118').length) {
						var headerHeight = $('header').outerHeight();
						$('.image-left').css('padding-top', headerHeight);
					}
					if ($('.single').length) {
						var headerHeight = $('header').outerHeight();
						$('.title-bar').css('padding-top', headerHeight);
					}
					if ($('.single').length) {
						var fullHeight = $(window).outerHeight(),
							headerHeight = $('header').outerHeight(),
							titleHeight = $('.title-bar').outerHeight(),
							remaining = (fullHeight - (titleHeight));
						$('.property-main, .single-bio').css('min-height', remaining);
					}
				}

				function bgTransition() {
					$(window).load(function () {
						$('.section-bg').each(function () {
							$(this).css('opacity', '1');
						});
					})
				}

				function paddingHelp() {
					if ($(window).width() > 768) {
						$('.image-left .content').each(function () {
							var containerWidth = $('.container').outerWidth(),
								windowWidth = $(window).outerWidth(),
								remainingPad = (windowWidth - containerWidth),
								half = (remainingPad / 2);
							$(this).css('padding-right', half);
						});
						$('.image-right .content').each(function () {
							var containerWidth = $('.container').outerWidth(),
								windowWidth = $(window).outerWidth(),
								remainingPad = (windowWidth - containerWidth),
								half = (remainingPad / 2);
							$(this).css('padding-left', half);
						});
					}
				}

				if ($('.page-id-95').length) {
					$(document).ready(function () {
						$('.properties-filter li a').not('#active-properties, #filter-cover').each(function () {
							var thisURL = $(location).attr('host'),
								link = $(this).attr('href'),
								slug = link.replace('/category/', ''),
								slug = slug.replace('https://' + thisURL, ''),
								slug = slug.replace('/', '');
							$(this).attr('href', '#' + slug);
						});
					});
					if ($(window).width() < 768) {
						$('.properties-filter li a').not('#active-properties, #filter-cover').click(function () {
							var link = $(this).attr('href'),
								link = link.replace('#', '');
							$('.single-portfolio-item').fadeOut(0);
							$('.single-portfolio-item[data-filter="' + link + '"]').fadeIn(0);
							$('.properties-filter li a').removeClass('active');
							$('.properties-filter li a').not('#filter-cover').css('display', 'none');
							$(this).addClass('active');
							return false;
						});
						$('#active-properties').click(function () {


							$('.single-portfolio-item').not('.active-property').fadeOut(0);
							$('.active-property').fadeIn(300);
							$('.properties-filter li a').removeClass('active');
							$('.properties-filter li a').not('#filter-cover').css('display', 'none');
							$(this).addClass('active');
							return false;
						});
					} else {
						$('.properties-filter li a').not('#active-properties, #filter-cover').click(function () {
							var link = $(this).attr('href'),
								link = link.replace('#', '');
							$('.portfolio-gallery').css('min-height', '50vh');
							$('.single-portfolio-item').fadeOut(300).delay(300);
							$('.single-portfolio-item[data-filter="' + link + '"]').fadeIn(300);
							$('.properties-filter li a').removeClass('active');
							$(this).addClass('active');
							return false;
						});
						$('#active-properties').click(function () {


							$('.single-portfolio-item').not('.active-property').fadeOut(300).delay(300);
							$('.active-property').fadeIn(300);
							$('.properties-filter li a').removeClass('active');
							$(this).addClass('active');
							return false;
						});


					}
					$('#filter-all').click(function () {
						$('.single-portfolio-item').fadeIn(300);
						$('.properties-filter li a').removeClass('active');
						$(this).addClass('active');
						return false;
					});
					$('#filter-cover').click(function () {
						$('.properties-filter li a').css('display', 'block');
						return false;
					});
				}



			},
			finalize: function () {
				// JavaScript to be fired on all pages, after page specific JS is fired

				if ($('.single-team').length) {

					var thisURL = window.location.href,
						root = window.location.hostname,
						slug = thisURL.replace('/team-member/', ''),
						slug = slug.replace('https://', ''),
						slug = slug.replace('/', ''),
						slug = slug.replace(root, '');

					$('#' + slug).addClass('current-member');

				}

			}
		}, // Home page
		'home': {
			init: function () {
				// JavaScript to be fired on the home page
				fullHero();

				function fullHero() {
					var fullHeight = $(window).outerHeight(),
						contentHeight = $('.hero .container').outerHeight(),
						remaining = (fullHeight - contentHeight),
						padding = (remaining / 2);
					$('.hero').css('padding-top', padding).css('padding-bottom', padding);
				}
				$(window).resize(function () {
					fullHero();
				})

					$('#scroll-me').click(function () {


						var target = $(this.hash);


						$('html, body').animate({
						  scrollTop: target.offset().top - 80
						}, 1000);
						return false;
					});

			},
			finalize: function () {
				// JavaScript to be fired on the home page, after the init JS
			}
		}, // About us page, note the change from about-us to about_us.
		'properties': {
			init: function () {

			}
		}
	};
	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function (func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';
			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function () {
			// Fire common init JS
			UTIL.fire('common');
			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});
			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};
	// Load Events
	$(document).ready(UTIL.loadEvents);
})(jQuery); // Fully reference jQuery after this point.
