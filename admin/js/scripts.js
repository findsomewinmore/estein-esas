(function ($) {

	$('.topic-handle').click(function () {

		var hash = $(this).parents('tr').next('.collapsed');

		$(this).parent('h2').addClass('active');
		$('.topic-handle').not(this).parent('h2').removeClass('active');
		$('.collapsed').not(hash).css('display','none');
		$(hash).css('display','table-row');
		return false;
	});


	$('.image-link').magnificPopup({type:'image'});

})(jQuery); // Fully reference jQuery after this point.