<?php
/**
 * The template for displaying Search Results pages
 *
 * @package Smores
 * @since Smores 2.0
 */
?>
    <?php get_template_part('templates/header');

		get_template_part( 'partials/single', 'bio');

		get_template_part( 'partials/footer', 'team');

		get_template_part('templates/footer'); ?>