<?php
// Example custom post type

 $properties = new CPT(
     array(
         'post_type_name' => 'properties',
         'singular'       => 'Property',
         'plural'         => 'Properties',
         'slug'           => 'properties',
     ),
     array(
         'supports' => array(
             'title'
         ),
         'public' => true,
         'show_ui' => true,
         'taxonomies'          => array( 'category' ),

     )
 );

 $properties->menu_icon("dashicons-grid-view");
