<?php
// Example custom post type

 $team = new CPT(
     array(
         'post_type_name' => 'team',
         'singular'       => 'Team Member',
         'plural'         => 'Team Members',
         'slug'           => 'team-member',
     ),
     array(
         'supports' => array(
             'title'
         ),
         'public' => true,
         'show_ui' => true,

     )
 );

 $team->menu_icon("dashicons-admin-users");
