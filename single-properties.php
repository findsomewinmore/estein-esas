<?php
/**
 * The template for displaying Search Results pages
 *
 * @package Smores
 * @since Smores 2.0
 */
?>
    <?php

			get_template_part('templates/header');

			get_template_part('partials/single', 'property');

//			get_template_part('partials/footer', 'active');


?>

        <?php get_template_part('templates/footer'); ?>