<?php ;?>

<section class="portfolio-gallery">
	<div class="overlay gradient-blue"></div>
    <?php if(get_sub_field('section_content')){?>
    <div class="container filter-row">
    <div class="row">

    <div class="col-sm-8 col-sm-offset-2 text-white mt32 mb32">

    <?php the_sub_field('section_content');?>

    </div>

    </div>
    </div>
    <?php }?>
	<div class="row m-xs-0">

		<?php $args = array(

                'posts_per_page' => -1,
                'orderby' => 'title' ,
                'order'   => 'ASC',
                'post_type' => 'properties',
				'meta_key' => 'active',
				'meta_value' => 'active-property'
            );

            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); $category = get_the_category(); ?>

			<?php $active = get_field('active');?>

				<div class="col-sm-3 col-xs-6 pt120 pb120 pt-xs-64 pb-xs-64 single-portfolio-item <?php if(!$active == null){?>active-property <?php }?>" data-filter="<?php echo sanitize_title_with_dashes($category[0]->cat_name);?>">
					<a href="<?php the_permalink();?>">
						<div class="section-bg" <?php if(get_field( 'main_image', $feature->ID)){?>style="background-image: url(
							<?php $attachment_id = get_field('main_image', $feature->ID); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"
								<?php }?>></div>
						<div class="over">
							<h5><?php echo get_the_title();?></h5>
							<h6 class="subhead"><?php echo $category[0]->cat_name;?></h6>

						</div>
						<div class="active-label">Active Property</div>
					</a>

				</div>

				<?php endwhile; wp_reset_postdata();?>

	</div>
</section>
