<?php ;?>

<section>


    <div class="row">

            <?php $args = array(

                'posts_per_page' => 4,
                'orderby' => 'date' ,
                'order'   => 'DESC',
                'post_type' => 'team');

            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="col-sm-4 pt240 pb120 single-headshot" id="<?php echo basename(get_permalink()); ?>">
                <a href="<?php the_permalink();?>">
                <div class="section-bg" style="background-image: url(<?php $attachment_id = get_field('headshot'); $size = " large "; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
                <div class="over">
                <h5><?php echo get_the_title();?></h5>
                <h6 class="subhead"><?php the_field('title');?></h6>
                </div>
                </a>
            </div>
<?php endwhile; wp_reset_postdata();?>

        </div>

    </section>