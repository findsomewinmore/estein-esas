<?php ;?>

<section>


    <div class="row m-xs-0">

        <?php $members = get_sub_field('team_members'); ?>
        <?php foreach( $members as $member ): ?>

            <div class="col-sm-3 col-ls-6 pt240 pb120 single-headshot">
                <a href="<?php the_permalink($member->ID);?>">
                <div class="section-bg" style="background-image: url(<?php $attachment_id = get_field('headshot', $member->ID); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
                <div class="over">
                <h5><?php echo get_the_title($member->ID);?></h5>
                <h6 class="subhead"><?php the_field('title', $member->ID);?></h6>
                </div>
                </a>
            </div>

        <?php endforeach; ?>

        </div>

    </section>