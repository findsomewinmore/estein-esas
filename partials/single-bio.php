<?php ;?>

        <section class="bg-white image-left">
            <div class="row">
                <div class="title-bar">
                <div class="section-bg" style="background-image: url(/wp-content/uploads/2017/01/Victory-Plaza-Office-Buildings.jpg)"></div>
                <div class="overlay blue"></div>
                    <div class="container">
                        <h1>Meet <?php echo get_the_title();?></h1> </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-4 match-me pt-sm-240 pb-sm-240 single-bio">
                <div class="section-bg" style="background-image: url(<?php $attachment_id = get_field('headshot', $member->ID); $size = " large "; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div></div>
                    <div class="col-md-8 match-me">
                        <div class="content">
                            <h2 class="mb16" id="member"><?php echo get_the_title();?></h2>
                            <h5 class="subhead"><?php the_field('title');?></h5>
                            <?php the_field('bio');?>
                        </div>
                    </div>
                </div>
        </section>