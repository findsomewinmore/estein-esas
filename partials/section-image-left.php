<?php ;?>

<section class="image-left">
            <div class="overlay gradient-blue"></div>

    <div class="row">
        <div class="col-md-6 match-me pt-sm-120 pb-sm-120">
            <div class="section-bg" style="background-image: url(<?php $attachment_id = get_sub_field('row_image'); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
        </div>


        <div class="col-md-6 match-me text-white">
            <div class="content">
                <h3><?php the_sub_field('headline');?></h3>
                <?php if(get_sub_field('section_content')) ; the_sub_field('section_content') ?>
                <?php if(get_sub_field('cta_link')) {?><a href="<?php $link = get_sub_field('cta_link'); the_permalink($link->ID);?>" class="btn btn-filled"><?php $link = get_sub_field('cta_link'); echo get_the_title($link->ID);?></a><?php }?>

            </div>
        </div>
    </div>
</section>