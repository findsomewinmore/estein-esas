<?php ;?>

<section class="portfolio-gallery">
            <div class="overlay gradient-blue"></div>

    <?php if(get_sub_field('section_content')){?>
    <div class="container filter-row">
    <div class="row">

    <div class="col-sm-8 col-sm-offset-2 text-white mt32 mb32">

    <?php the_sub_field('section_content');?>

    </div>

    </div>
    </div>
    <?php }?>
    <?php if(is_page('properties')){?>
    <div class="container">
    <div class="row pb-xs-8 pt-xs-8">

    <div class="col-sm-12 text-white mt32 mb32 mt-xs-0 mb-xs-0">

                                <ul class="properties-filter">
                                    <li class="visible-xs"><a href="#" id="filter-cover"><i class="fa fa-filter"></i> Filter Properties</a></li>
                                <!--<li>
									<a href="#" id="active-properties" class="">
										Active Properties
									</a>
								</li>-->


                                <li>
									<a href="#" id="filter-all" class="active">
										All Properties
									</a>
								</li>
                                    <?php wp_list_categories( array(
                                        'orderby' => 'name',
                                        'title_li' => '',
                                        'hide_empty' => 1,
                                    ) ); ?>




                                </ul>

    </div>

    </div>
    </div>
    <?php }?>


    <div class="row m-xs-0">


            <?php $args = array(

                'posts_per_page' => -1,
                'orderby' => 'title' ,
                'order'   => 'ASC',
                'post_type' => 'properties',
				'meta_key'=> 'order',
				'orderby' => array(
									'meta_value' => 'ASC',
									'title' => 'ASC'
				),
            );



            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); $category = get_the_category(); ?>

			<?php $active = get_field('active');?>

            <div class="col-sm-3 col-xs-6 pt120 pb120 pt-xs-64 pb-xs-64 single-portfolio-item <?php echo $active;?>" data-filter="<?php echo sanitize_title_with_dashes($category[0]->cat_name);?>">
                <a href="<?php the_permalink();?>">
                <div class="section-bg" <?php if(get_field('main_image', $feature->ID)){?>style="background-image: url(<?php $attachment_id = get_field('main_image', $feature->ID); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)" <?php }?>></div>
                <div class="over">
                <h5><?php echo get_the_title();?></h5>
                <h6 class="subhead"><?php echo $category[0]->cat_name;?></h6>
                </div>
<!--				<div class="active-label">Active Property</div>-->
                </a>
            </div>

            <?php endwhile; wp_reset_postdata();?>




        </div>
    </section>
