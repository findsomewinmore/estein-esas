<?php ;?>

	<section class="bg-white">
		<div class="row">
			<div class="title-bar">
				<div class="section-bg" style="background-image: url(/wp-content/uploads/2017/01/Victory-Plaza-Office-Buildings.jpg)"></div>
				<div class="overlay blue"></div>
				<div class="container">
					<h1><?php echo get_the_title();?></h1> </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 match-me pt-sm-240 pb-sm-240 property-main">
				<div class="section-bg" <?php if(get_field( 'main_image')){?>style="background-image: url(
					<?php $attachment_id = get_field('main_image'); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"
						<?php }?>></div>
			</div>
			<div class="col-md-4 visible-sm visible-xs">
				<div class="content">
					<h2 class="mb16"><?php echo get_the_title();?></h2>
					<h5 class="subhead"><?php the_field('asset_type');?></h5>

					<div class="prop-details">
						<?php the_field('property_description');?>
					</div>

				</div>
			</div>
			<div class="col-md-8 match-me pt-sm-120 pb-sm-120">
				<div class="content over-map hidden-sm hidden-xs">
					<h2 class="mb16"><?php echo get_the_title();?></h2>
					<h5 class="subhead"><?php the_field('asset_type');?></h5>

					<div class="prop-details">
						<?php the_field('property_description');?>
					</div>

				</div>
				<div id="single-map"></div>
			</div>
		</div>
	</section>
