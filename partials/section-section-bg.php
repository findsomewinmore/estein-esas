<?php ;?>

        <section class="section-bg-container pt16 pb16" style="background-image: url(<?php $attachment_id = get_sub_field('section_bg'); $size = "banner"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)">
            <div class="overlay gradient-blue"></div>

            <?php if( have_rows('section_row') ):?>
            <?php $counter = 0; while ( have_rows('section_row') ) : the_row(); $counter++;?>
            <?php if( $counter % 2 == 0 ) {?>

                    <div class="row pt32 pb32 pt-sm-16 pb-sm-16 image-right">

                     <div class="col-md-push-6 col-md-6 match-me pt-sm-120 pb-sm-120">
                            <div class="section-bg shadow-1" style="background-image: url(<?php $attachment_id = get_sub_field('row_image'); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
                        </div>


                        <div class="col-md-pull-6 col-md-6 pt64 pb64 pt-xs-0 pb-xs-0 match-me text-white">
                            <div class="content">
                                <h3><?php the_sub_field('headline');?></h3>
                                <?php the_sub_field('section_content');?>
                <?php if(get_sub_field('cta_link')) {?><a href="<?php $link = get_sub_field('cta_link'); the_permalink($link->ID);?>" class="btn btn-filled"><?php $link = get_sub_field('cta_link'); echo get_the_title($link->ID);?></a><?php }?>

                            </div>
                        </div>





                    </div>

            <?php } else {?>

                        <div class="row pt32 pb32 pt-sm-16 pb-sm-16 image-left">


                            <div class="col-md-6 match-me pt-sm-120 pb-sm-120">
                                <div class="section-bg shadow-1" style="background-image: url(<?php $attachment_id = get_sub_field('row_image'); $size = "large"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
                            </div>

                            <div class="col-md-6 match-me pt64 pb64 pt-xs-0 pb-xs-0 text-white text-right">
                                <div class="content">
                                    <h3><?php the_sub_field('headline');?></h3>
                                    <?php the_sub_field('section_content');?>
                <?php if(get_sub_field('cta_link')) {?><a href="<?php $link = get_sub_field('cta_link'); the_permalink($link->ID);?>" class="btn btn-filled"><?php $link = get_sub_field('cta_link'); echo get_the_title($link->ID);?></a><?php }?>

                                </div>
                            </div>

                        </div>
            <?php }?>



            <?php endwhile; ?>
            <?php endif; ?>


        </section>
