<?php ;?>

        <section class="pt180 pb120 hero">
            <div class="section-bg" style="background-image: url(<?php $attachment_id = get_sub_field('hero_image'); $size = "banner"; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
            <div class="overlay"></div>

            <div class="container">

                <div class="<?php the_sub_field('alignment');?> <?php the_sub_field('text_alignment');?> text-white">
                <h1 class="m0"><?php the_sub_field('headline');?></h1>
                <?php if(get_sub_field('hero_content')) ; the_sub_field('hero_content') ?>
                <?php if(get_sub_field('cta_link')) {?><a href="<?php $link = get_sub_field('cta_link'); the_permalink($link->ID);?>" class="btn btn-filled"><?php $link = get_sub_field('cta_link'); echo get_the_title($link->ID);?></a><?php }?>

                </div>
            </div>

			<?php if(is_page(2)): echo '<a href="#home" id="scroll-me"><span class="roll-down"></span></a>'; endif;?>
        </section>
		<?php if(is_page(2)): echo '<div id="home"></div>'; endif;?>