<?php ;?>

<section>
            <div class="overlay gradient-blue"></div>

    <div class="row">
        <div class="col-md-6 match-me pt-sm-180 pb-sm-180">
            <div id="map"></div>
        </div>


        <div class="col-md-6 match-me text-white">
            <div class="content">
                <h3><?php the_sub_field('headline');?></h3>
                <?php if(get_sub_field('section_content')) ; the_sub_field('section_content') ?>

                <?php the_sub_field('contact_form');?>

            </div>
        </div>
    </div>
</section>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVqVn0srESlbUkaPoDNi7GQJTLfKoUZns"></script>

                            <script type="text/javascript">
                                function mapinitialize() {
                                    var latlng = new google.maps.LatLng(28.4925246,-81.5077787);
                                    var myOptions = {
                                        zoom: 16,
                                        center: latlng,
                                        scrollwheel: false,
                                        scaleControl: false,
                                        disableDefaultUI: false,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        // Google Map Color Styles
                                        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":20},{"color":"#ececec"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"on"},{"color":"#f0f0ef"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#f0f0ef"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#d4d4d4"}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"on"},{"color":"#ececec"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"lightness":21},{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#d4d4d4"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#303030"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"geometry.stroke","stylers":[{"lightness":"-61"},{"gamma":"0.00"},{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#dadada"},{"lightness":17}]}]
                                    };
                                    var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                  var image = "/wp-content/themes/eistein-esas/dist/img/pin.png";
                                    var image = new google.maps.MarkerImage("/wp-content/themes/eistein-esas/dist/img/pin.png", null, null, null, new google.maps.Size(100, 99));
                                    var marker = new google.maps.Marker({
                                        map: map,
                                        icon: image,
                                        position: map.getCenter()
                                    });

                                    var contentString = '<div class="text-center"><h2 class="mt8 mb0">Estein USA</h2><p class="mt8">4705 S Apopka Vineland Rd Suite 201 <br>Orlando, FL 32819<br><a href="tel:<?php the_field('phone','options');?>"><?php the_field('phone','options');?></a></p></div>';
                                    var infowindow = new google.maps.InfoWindow({
                                        content: contentString
                                    });
                                    google.maps.event.addDomListener(window, "resize", function() {
                                        var center = map.getCenter();
                                        google.maps.event.trigger(map, "resize");
                                        map.setCenter(center);
                                    });


                                    setTimeout(function() {
                                        infowindow.open(map, marker);

                                    }, 750)
                                }

                                            mapinitialize();

                            </script>