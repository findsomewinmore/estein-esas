<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>


<?php // check if the flexible content field has rows of data
if( have_rows('section') ):

     // loop through the rows of data
    while ( have_rows('section') ) : the_row();

        if( get_row_layout() == 'large_hero' ):

        	get_template_part( 'partials/section', 'hero' );

		endif;

        if( get_row_layout() == 'split_image_right' ):

        	get_template_part( 'partials/section', 'image-right' );

		endif;

        if( get_row_layout() == 'split_image_left' ):

        	get_template_part( 'partials/section', 'image-left');

		endif;

         if( get_row_layout() == 'section_bg' ):

            get_template_part( 'partials/section', 'section-bg' );

		endif;

         if( get_row_layout() == 'portfolio_gallery' ):

            get_template_part( 'partials/section', 'gallery' );

		endif;

//         if( get_row_layout() == 'active_properties' ):
//
//            get_template_part( 'partials/footer', 'active' );
//
//		endif;

         if( get_row_layout() == 'members_gallery' ):

            get_template_part( 'partials/section', 'team' );

		endif;

        if( get_row_layout() == 'contact_form' ):

            get_template_part( 'partials/section', 'contact' );

        endif;

    endwhile;

endif;

?>

<?php get_template_part('templates/footer'); ?>
