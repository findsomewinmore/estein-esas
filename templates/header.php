<?php
get_template_part('templates/head');
?>

<div class="mobile-menu visible-xs">

        <?php wp_nav_menu( array( 'theme_location' => 'main' ) ); ?>
</div>

    <header class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-ls">
                    <div class="logo">
                        <a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo-full-white.svg" alt="<?php bloginfo( 'name' ); ?>"></a>
                    </div>
                </div>
                <div class="col-md-8 hidden-xs visible-ls">
                    <div class="navigation">
                        <?php wp_nav_menu( array( 'theme_location' => 'main' ) ); ?>

                    </div>
                </div>
            </div>
        </div>
    </header>

