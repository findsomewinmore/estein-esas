<?php
/**
 * The template for displaying the footer
 *
 * @package Smores
 * @since Smores 2.0
 */
?>
    <!-- Asynchronous Javascript Loacing Example -->
    <!-- <script type="text/javascript">
	    (function(d, t) {
	        var g = d.createElement(t),
	            s = d.getElementsByTagName(t)[0];
	        g.src = '[path/to/script]';
	        s.parentNode.insertBefore(g, s);
	    }(document, 'script'));
	</script>-->





    <footer class="pt32 pb32 text-white">
        <div class="section-bg" style="background-image: url(<?php $attachment_id = get_field('footer_bg','options'); $size = " banner "; $image = wp_get_attachment_image_src( $attachment_id, $size ); echo $image[0];?>)"></div>
        <div class="overlay blue"></div>
        <div class="container">
            <div class="row mb32">
                <div class="col-sm-4 col-tab-port-6">
                    <div class="footer-logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo-full-white.svg" alt="<?php bloginfo( 'name' ); ?>">
                        <div class="hidden-xs">
                            <?php wp_nav_menu( array( 'theme_location' => 'main' ) ); ?>
                        </div>

                    </div>


                </div>
                <div class="col-sm-4 col-tab-port-6 tx-xs-center">
                    <h4 class="mt32">Office Location</h4>
                    <p>
                        <?php the_field('address','options');?>
                    </p>
                    <p>
                        <?php the_field('city','options');?>,
                        <?php the_field('state','options');?>
                        <?php the_field('zip','options');?>
                    </p>
                    <br>
                    <p>
                        <?php the_field('phone','options');?>
                    </p>


                </div>


            </div>

            <div class="row">
                <div class="col-sm-4">
                    <p class="p0 tx-xs-center mb-xs-16">Copyright
                        <?php echo date("Y"); ?> Estein USA</p>

                </div>
                <div class="col-sm-4 col-sm-offset-4">
                    <a href="https://findsomewinmore.com/" target="blank" rel="nofollow"><img class="by-fiwi" src="<?php echo get_template_directory_uri(); ?>/dist/img/FIWI-classic-website-by-white.svg"></a>
                </div>
            </div>


        </div>


    </footer>


    <div class="visible-xs mobile-toggle">
        <a class="shadow-2" href="#"><span>Menu</span></a>

    </div>
    <div class="visible-xs dummy" href="#"></div>
    <?php wp_footer(); ?>

    <?php if(is_singular('properties')){?>

    <?php get_template_part( 'partials/script', 'single-property' ); ?>

    <?php }?>


    </body>

    </html>
